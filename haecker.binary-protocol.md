# Binary Protocol

This is a standard protocol used by multiple Haecker Automation components. It follows a request-response/remote-procedure-call model and is most commonly used in client-server systems. The binary packages are designed to be minimal, so the protocol is optimized to be used in time- and performance-critical applications. Resulting binary packages can be delivered via any transport layer, most commonly network/TCP.

## Specification

#### Protocol Ident

The first part of any package is the identifier of the specific protocol implementation. It is of variable size and will most often be a human-readable string encoded as UTF-8. There will be two identifiers for each protocol implementation, one for the requests and one for the responses. The package identifier will simply denote that the next X bytes compose a binary protocol message of a given protocol implementation, and should not be considered part of the actual message for any other purpose.

#### Header

Each message will have a header consisting of 32 bytes total. It is composed of the fields in order:

+ ``CommandType`` (``long``, 8 bytes) declares what procedure is to be executed. It will be chosen by the request and simply returned by the response.
+ ``CommandIdent`` (``GUID``, 16 bytes) is a unique identifier set by the request and simply returned by the response. It will map map each request to its corresponding response.
+ ``CommandState`` (``int``, 4 bytes) is the state code of the request/response, analogous to a status code in HTTP. All error codes should be negative, whereas request and success codes should be positive. All codes will be defined specific to a protocol implementation, except for the the following standard codes:
    + ``REQUEST = 0``
    + ``OK = 1``
    + ``FAIL = -1``
    + ``FAILPROCESSING = -2``
+ ``CommandLength`` (``int``, 4 bytes) is the total length of the following data field in bytes. If there is no data, it should be ``0``.

#### Data field

The data field is for any command-specific input or output data. How to parse/interpret this data depends on the ``CommandType``, and can also depend on the ``CommandState`` or wether its a request or response.
For instance, if the ``CommandState`` indicates a certain error, the data field could hold data specific to that error.

#### Summary

To summarize, a full message is composed as follows:

| Protocol Identifier | Header   | Data          |
| :-----------------: | :------: | :-----------: |
| Variable Size       | 32 Bytes | Variable Size |

Whereas the header is composed as follows:

| CommandType | CommandIdent | CommandState | CommandLength |
| :---------: | :----------: | :----------: | :-----------: |
| ``long``    | ``GUID``     | ``int``      | ``int``       |
| 8 Bytes     | 16 Bytes     | 4 Bytes      | 4 Bytes