"""
binary_protocol.py
"""

import asyncio
import struct
from typing     import Any
from enum       import IntEnum
from abc        import abstractmethod


class NotMatchingIdentError(Exception):
    """Exception thrown on parsed incoming data not matching pre-determined packet identifier"""
    pass


class UnknownCommandTypeError(Exception):
    """Exception thrown on an undefined command type being parsed"""
    pass


class AttributeListLengthError(Exception):
    """Exception thrown when parameter class is given a attribute list of length that isn't matching itself"""
    pass


class CommandFailError(Exception):
    """Thrown on any otherwise uncaught exception during command execution"""
    pass


class CommandFailProcessingError(Exception):
    """Thrown during parsing error"""
    pass


class CommandStates(IntEnum):
    """Base command states enumeration, extend in implementing class for further protocol-specific constants"""

    REQUEST        = 0
    OK             = 1
    FAIL           = -1
    FAILPROCESSING = -2


class BinaryProtocolMessageHeader:
    """Data record for header"""

    SIZE = 32  # size constant in bytes

    def __init__(self, *,
        command_type   : int       = None,
        command_ident  : str       = None,
        command_state  : int       = None,
        command_length : int       = None,
        from_bytes     : bytearray = None
    ):
        # parse header data from bytes if given
        if from_bytes is not None:
            self.from_bytes(from_bytes)
        # otherwise use explicit data
        else:
            self.command_type   = command_type    # API identifier, designates which command is called
            self.command_ident  = command_ident   # GUID of command
            self.command_state  = command_state   # TODO ?
            self.command_length = command_length  # length of entire message in number of bytes

    def from_bytes(self, raw: bytearray):
        """Parses header data from bytearray"""
        data = struct.unpack('=q16sii', raw)
        self.command_type   = data[0]
        self.command_ident  = data[1]
        self.command_state  = data[2]
        self.command_length = data[3]

    def to_bytes(self) -> bytearray:
        """Returns binary representation of header"""
        return struct.pack(
            '=q16sii',
            self.command_type,
            self.command_ident,
            self.command_state,
            self.command_length
        )


class BinaryProtocolMessageParams(list):
    """Data record for params"""

    def __init__(self, *,
        from_list  : list      = None,
        from_bytes : bytearray = None,
        type_list  : list      = None,
        attr_list  : list      = None
    ):
        if type_list: self._types = type_list
        else: self._types = []
        # create from explicit list if given
        if from_list is not None:
            super().__init__(from_list)
        # parse from bytes otherwise
        else:
            self.from_bytes(from_bytes)
        # parse attr names
        if not(attr_list is None or attr_list == []):
            if len(attr_list) != len(self): raise AttributeListLengthError
            else: self.parse_attributes(attr_list)

    def from_bytes(self, raw: bytearray):
        """Parses param data from bytearray"""
        param_list = []
        param_tuple = struct.unpack("=" + "".join(self._types), raw)
        for ele in param_tuple:
            param_list.append(ele)
        super().__init__(param_list)

    def to_bytes(self) -> bytearray:
        """Returns binary representation of params"""
        return struct.pack("=" + "".join(self._types), *self)

    def parse_attributes(self, attr_list: list):
        """Parses attribute names for params"""
        for i in range(0, len(self)):
            self.__setattr__(attr_list[i], self[i])


class BinaryProtocolMessage:
    """Data record for entire protocol message"""

    def __init__(self,
        *,
        identifier : str,
        header     : BinaryProtocolMessageHeader,
        parameters : BinaryProtocolMessageParams
    ):
        self.identifier = identifier  # protocol-specific identifier constant, dynamic length and excluded from length field
        self.header     = header      # message header
        self.parameters = parameters  # optional parameters

    def to_bytes(self) -> bytearray:
        """Returns binary representation of entire message"""
        return bytearray(self.identifier.encode('utf8'))    \
            + self.header.to_bytes()                        \
            + self.parameters.to_bytes()

    def evaluate_length_field(self):
        """Explicitly sets headers length field from param byte size"""
        self.header.command_length = len(self.parameters.to_bytes())


class BinaryProtocolComponent:
    """Common components of server and client"""

    def __init__(self, recv_ident, send_ident) -> None:
        self.recv_ident = recv_ident
        self.send_ident = send_ident


class BinaryProtocolClient(BinaryProtocolComponent):
    """Base BinaryProtocolClient, offering reader and writer streams for TCP connection"""

    PARAMETER_MAP = {}  # dict mapping command types (int) to string lists of struct types, abstract
    ATTRIBUTE_MAP = {}  # dict mapping command types (int) to string list of attribute names of the positional parameters arising from PARAMETER_MAP (optional)

    def __init__(self,
        reader     : asyncio.StreamReader,
        writer     : asyncio.StreamWriter,
        recv_ident : str,
        send_ident : str
    ):
        super().__init__(recv_ident, send_ident)
        self.reader  = reader
        self.writer  = writer
        self.running = False

    async def run(self):
        """
        Run client on infinite loop, start as coroutine.
        Await incoming data and decode it, ultimately firing the abstract on_message event
        """
        self.running = True
        while self.running:
            try:
                msg_data = []
                # get ident
                ident_raw : bytes = await self.reader.readexactly(len(self.recv_ident))
                ident     : str   = ident_raw.decode('utf8')
                if ident != self.recv_ident:
                    raise NotMatchingIdentError
                else:
                    msg_data.append(ident)
                try:
                    # ident matching, get header
                    header_raw : bytes = await self.reader.readexactly(BinaryProtocolMessageHeader.SIZE)
                    header     : Any   = BinaryProtocolMessageHeader(from_bytes=bytearray(header_raw))
                    msg_data.append(header)
                except Exception as error:
                    print(f"Fatal exception during header parse: {error}")
                    raise CommandFailError
                try:
                    # get params
                    msg_data.append(await self._get_params(header))
                except Exception as error:
                    print(f"Fatal exception during parameter parse: {error}")
                    raise CommandFailProcessingError
                # construct message
                response = self.on_message(
                    BinaryProtocolMessage(
                        identifier = msg_data[0],
                        header     = msg_data[1],
                        parameters = msg_data[2]
                    )
                )
                # respond message back to client if implementing user returned one
                if response is not None:
                    self.write_message(response)
            except CommandFailProcessingError:
                self.write_message(BinaryProtocolMessage(
                    identifier = self.send_ident,
                    header     = header,
                    parameters = BinaryProtocolMessageParams(from_list=[])
                ))
            except (NotMatchingIdentError, CommandFailError):
                # drain buffer
                try: await self.reader.read(-1)
                except asyncio.IncompleteReadError: pass
            except (ConnectionResetError, KeyboardInterrupt, asyncio.IncompleteReadError):
                # stop loop and 'destroy' self
                self.is_running = False
                self.writer.close()
                del self.reader
                del self.writer
                break

    async def _get_params(self, header: BinaryProtocolMessageHeader):
        """
        Get params of a message from the given header.
        Only support static parameter size, needs to be reimplemented in inheriting class for dynamic sizes.
        """
        # return empty params if length 0
        if header.command_length == 0:
            return BinaryProtocolMessageParams(from_list=[], type_list=[])
        # read out otherwise
        else:
            raw = await self.reader.readexactly(header.command_length)
            return self._build_params(raw, header)

    def _build_params(self, data: bytes, header: BinaryProtocolMessageHeader) -> BinaryProtocolMessageParams:
        """Private wrapper for building params, overwrite for custom parameter classes"""
        return BinaryProtocolMessageParams(
                from_bytes  = data,
                type_list   = self.PARAMETER_MAP.get(header.command_type, None),
                attr_list   = self.ATTRIBUTE_MAP.get(header.command_type, None)
            )

    def write_message(self, msg: BinaryProtocolMessage):
        """Write message on writer stream"""
        msg.identifier = self.send_ident
        self.writer.write(
            msg.to_bytes()
        )

    @abstractmethod
    def on_message(self, msg: BinaryProtocolMessage) -> BinaryProtocolMessage:
        """
        Event fired on message received and decoded, implemented in protocol-specific client.
        Automatically replies if implementing user returns a BinaryProtocolMessage from on_message().
        """
        pass


class BinaryProtocolServer(BinaryProtocolComponent):
    """Base BinaryProtocolServer, hosting TCP server which creates corresponding client object on connections"""

    CLIENT_CLASS = BinaryProtocolClient  # abstract, reference to client class which is created upon dispatch

    def __init__(self,
        ip          : str,
        port        : int,
        recv_ident  : str,
        send_ident  : str
    ):
        super().__init__(recv_ident, send_ident)
        self.ip     = ip
        self.port   = port

    def listen(self, loop: asyncio.AbstractEventLoop):
        """Start server on given event loop"""
        self.loop = loop
        loop.run_until_complete(self._listen())

    async def _listen(self):
        """Private listen function called by public wrapper"""
        self.asyncserver = await asyncio.start_server(self._handle_connection, self.ip, self.port)
        await self.asyncserver.serve_forever()

    async def _handle_connection(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        """Private connection handler"""
        client : BinaryProtocolClient = self.CLIENT_CLASS(reader, writer, self.recv_ident, self.send_ident)
        self.on_connection(client)
        self.loop.create_task(client.run())

    @abstractmethod
    def on_connection(
        self,
        client: BinaryProtocolClient
    ):
        """Connection callback"""
        pass
