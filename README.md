This Python package implements the Häcker Binary Protocol. The specification of the protocol itself can be found [here](https://gitlab.com/ourplant.net/elektronik/microservice/alignment/binary-protocol/-/blob/main/haecker.binary-protocol.md).

### Installation

The package is contained in a single file ``binary_protocol.py`` with no external dependencies. It can simply be downloaded and added to anywhere Python looks for packages, so either a dir in ``$PYTHONPATH`` or the working directory of the given project.

### Usage

You need to implement the ``BinaryProtocolClient`` class by initializing ``PARAMETER_MAP`` and implementing the ``on_message`` method with your domain-specific requirements.

```python
class SomeProtocolClient(BinaryProtocolClient):

    PARAMETER_MAP = {
        10 : [],  # the command with ID 10 takes no arguments
        12 : ['d', 'i']  # the command with ID 12 takes a double and an integer as arguments
    }

    def on_message(self, msg: BinaryProtocolMessage):
        """Mock implementation for a server."""

        def _on_c1():
            return do_smth(msg.parameters)

        def _on_c2():
            return do_smth_else(msg.parameters)

        # switch over local functions that implement each command
        type_switch = {
            10 : _on_c1,
            12 : _on_c2
        }
        callback = type_switch[msg.header.command_type]
        msg.parameters = callback(msg.parameters)
        # whatever BinaryProtocolMessage is returned will be send back to the caller
        return msg
```

A client can then be started by calling/awaiting the ``run`` method.

Alternatively, you can start a server by creating a server class and giving it ``CLIENT_CLASS``, which is the class that will be instantiated for each new connection to the server.

```python
class SomeProtocolServer(BinaryProtocolServer):

    CLIENT_CLASS = SomeProtocolClient
```

For an example of a protcol implementation, you can check the one of the Mirror Alignment Service [here](https://gitlab.com/ourplant.net/elektronik/microservice/alignment/mirror-alignment-service/-/blob/main/protocol.py).
